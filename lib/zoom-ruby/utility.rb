module ZoomRuby
  class Utility
    def self.extract_options(options_array)
      options_array.last.is_a?(::Hash) ? options_array.pop : {}
    end


    def self.parse_response(http_response)
      response = http_response.parsed_response
      # Mocked response returns a string
      response.kind_of?(Hash) ? response : JSON.parse(response)
    end

    def self.define_bang_methods(klass)
      klass.instance_methods.each do |m|
        klass.send(:define_method, "#{m}!") do |*args|
          begin
            response = send(m, *args)
            ZoomRuby::Utility.raise_if_error!(response)
          rescue Net::OpenTimeout, Net::ReadTimeout, Timeout::Error => _e
            raise #::Zoomus::GatewayTimeout.new
          end
        end
      end
    end

    def self.raise_if_error!(response)
      if response["error"]
        raise Error.new(response["error"]["message"])
      else
        response
      end
    end

    def self.process_datetime_params!(params, options)
      params = [params] unless params.is_a? Array
      params.each do |param|
        if options[param] && options[param].kind_of?(Time)
          options[param] = options[param].strftime("%FT%TZ")
        end
      end
      options
    end
  end
end