class ZoomMeetingsController < ApplicationController
  def join
    encrypted_params = params[:data]
    decrypted_params = Marshal.load(Base64.strict_decode64(encrypted_params))
    @meeting_number = get_decrypted_data_param(decrypted_params, :meeting_number)
    @username = get_decrypted_data_param(decrypted_params, :username) || "Participant-#{rand(10000)}"
    @password = get_decrypted_data_param(decrypted_params, :password)
    @leave_url = get_decrypted_data_param(decrypted_params, :leave_url) || request.referer
    @role = get_decrypted_data_param(decrypted_params, :role) || '0'

    @zoom_client = ZoomRuby.new
    @signature = @zoom_client.get_signature({meeting_number: @meeting_number, password: @password, role: @role})
    @apikey = ZoomRuby.configuration.api_key
  end

  def get_decrypted_data_param(decrypted_params, key)
    (decrypted_params[key] || params[key]).to_s
  end

end