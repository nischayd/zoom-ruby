Rails.application.routes.draw do
  get 'meetings/join' => 'zoom_meetings#join', as: :join
  get 'zoom_meetings/join' => 'zoom_meetings#join', as: :zoom_join
end
